import { Injectable } from '@angular/core';

import {Http, Headers }  from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class GithubService {

    private username="bofiss";
    private client_id ="6ba62db23dad7853ad82";
    private client_secret ="4f400e901a153775e1906d1cf008c56b28f84b7b";
    constructor(private _http:Http) {
        console.log('Github Service Started...');
     }

     getUser() {
         return this._http.get('https://api.github.com/users/'+this.username+'?client_id='
         +this.client_id+'&client_secret='+this.client_secret)
             .map(res => res.json());
     }

     getRepos() {
         return this._http.get('https://api.github.com/users/'+this.username+'/repos?client_id='
         +this.client_id+'&client_secret='+this.client_secret)
             .map(res => res.json());
     }

     updateUsername(username:any){
         this.username = username;
     }
}